/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fibraoptica.controller;

import fibraoptica.model.AppData;
import fibraoptica.model.Bainha;
import fibraoptica.model.BainhaInvalidaException;
import fibraoptica.model.MeioAmbiente;
import fibraoptica.model.Nucleo;
import fibraoptica.model.FibraOptica;
import fibraoptica.ui.MainUI;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 *
 * @author dnamorim
 */
public class SimularExperienciaController {

    private AppData appdata;
    private FibraOptica fibra;

    public SimularExperienciaController(AppData appdata) {
        this.appdata = appdata;
    }

    public Object[] getItemsNucleo() {
        List<Item<Nucleo>> lstn = new ArrayList();
        for (Nucleo n : this.appdata.getListNucleos()) {
            lstn.add(new Item<>(n));
        }

        return lstn.toArray();
    }
    
    public FibraOptica getFibraOptica() {
        return this.fibra;
    }

    public Object[] getItemsBainha() {
        List<Item<Bainha>> lstb = new ArrayList();
        for (Bainha b : this.appdata.getListBainhas()) {
            lstb.add(new Item<>(b));
        }

        return lstb.toArray();
    }

    public Object[] getItemsMeioAmbiente() {
        List<Item<MeioAmbiente>> lstma = new ArrayList();
        for (MeioAmbiente ma : this.appdata.getListMeiosAmbiente()) {
            lstma.add(new Item<>(ma));
        }

        return lstma.toArray();
    }

    public void buildFiber(Object nucleo, Object bainha, Object meio) {
        Nucleo n = ((Item<Nucleo>) nucleo).getItem();
        Bainha b = ((Item<Bainha>) bainha).getItem();
        MeioAmbiente m = ((Item<MeioAmbiente>) meio).getItem();
        
        this.fibra = new FibraOptica(n, b, m);
    }
    
    public void resetFiber() {
        this.fibra = null;
    }
    
    public void setAnguloIncidencia(String angulo) throws NumberFormatException {
        double angIncidencia = Double.parseDouble(angulo);
        this.fibra.setAngIncidencia(angIncidencia);
    }
    
    public void efectuarCalculos() {
        this.fibra.efetuarCalculos();
    }
    
    public double getAnguloCritico() {
        return this.fibra.getAngCritico();
    }
    
    public double getAnguloAceitacao() {
        return this.fibra.getAngAceitacao();
    }
    
    public double getAberturaNumerica() {
        return this.fibra.getAberturaNumerica();
    }
    
    public double getAnguloRefractado() {
        return this.fibra.getAngRefractado();
    }
    
    public double getConeAceitacao() {
        return this.fibra.getConeAceitacao();
    }
    
    public boolean hasReflexaoTotal() {
        return this.fibra.hasReflexaoTotal();
    }
    
    public double getAnguloRefractadoEntrada() {
        return this.fibra.calcAngRefEnt();
    }
    
    public double getAnguloReflectido() {
        return this.fibra.calcAngRefIncidente();
    }

    public boolean hasRefValues() {
        return !(this.appdata.getListBainhas().isEmpty() && this.appdata.getListNucleos().isEmpty() && this.appdata.getListMeiosAmbiente().isEmpty());
    }

    public class Item<E> {

        private E it;

        public Item(E e) {
            this.it = e;
        }

        public Item() {
            this.it = null;
        }

        public E getItem() {
            return it;
        }

        public void setItem(E e) {
            this.it = e;
        }

        @Override
        public String toString() {
            if (it instanceof Nucleo) {
                Nucleo n = (Nucleo) it;
                 
                if(MainUI.language.getLocale().equals(Locale.UK)) {
                   return String.format("%s (%.3f)", n.getNomeEng(), n.getIndex());
                } else {
                    return String.format("%s (%.3f)", n.getNomePT(), n.getIndex());
                }
            }

            if (it instanceof Bainha) {
                Bainha b = (Bainha) it;
                if(MainUI.language.getLocale().equals(Locale.UK)) {
                   return String.format("%s (%.3f)", b.getNomeEng(), b.getIndex());
                } else {
                    return String.format("%s (%.3f)", b.getNomePT(), b.getIndex());
                }
                
            }

            if (it instanceof MeioAmbiente) {
                MeioAmbiente a = (MeioAmbiente) it;
                if(MainUI.language.getLocale().equals(Locale.UK)) {
                   return String.format("%s (%.3f)", a.getNomeEng(), a.getIndex());
                } else {
                    return String.format("%s (%.3f)", a.getNomePT(), a.getIndex());
                }
                
            }

            return it.toString();
        }
    }

}
