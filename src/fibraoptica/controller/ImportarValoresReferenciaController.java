/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fibraoptica.controller;

import fibraoptica.model.AppData;
import fibraoptica.model.XMLRefValuesParser;
import java.io.IOException;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;

/**
 *
 * @author dnamorim
 */
public class ImportarValoresReferenciaController {
    
    private AppData appdata;
    private XMLRefValuesParser xmlp;
    
    public ImportarValoresReferenciaController(AppData appdata) {
        this.appdata = appdata;
    }
    
    public void setFicheiroAImportar(String pathFile) throws ParserConfigurationException, SAXException, IOException {
        xmlp = new XMLRefValuesParser(appdata, pathFile);
    }
    
    public void importarValores() {
        xmlp.importCores();
        xmlp.importCladdings();
        xmlp.importEnvironments();
    }
    
    public boolean validaImportacao() {
        return (!this.appdata.isEmpty());
    }
}
