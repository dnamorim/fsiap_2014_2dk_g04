/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fibraoptica.controller;

import fibraoptica.model.AppData;
import fibraoptica.model.XMLValuesWriter;
import java.io.FileNotFoundException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * UC05: Exportar valores de referência para ficheiro XML
 *
 * @author Sara Freitas
 */
public class ExportarValoresReferenciaController {

    private AppData appdata;
    private XMLValuesWriter xmlw;
    
    public ExportarValoresReferenciaController(AppData appdata)  {
        this.appdata = appdata;
    }
    
    public boolean startWriter(String path) {
        try {
            this.xmlw = new XMLValuesWriter(path);
        } catch (FileNotFoundException ex) {
            return false; //something went wrong when opening the provided file
        }
        return true;
    }

    public void writeValues() {
        this.xmlw.writeMaterials(this.appdata.getListNucleos(), this.appdata.getListBainhas());
        this.xmlw.writeEnvironments(this.appdata.getListMeiosAmbiente());
    }
    
    public void endWriter() {
        this.xmlw.endWritter();
    }
 
}
