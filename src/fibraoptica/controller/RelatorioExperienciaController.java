/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fibraoptica.controller;

import fibraoptica.model.AppData;
import fibraoptica.model.FibraOptica;
import fibraoptica.model.HTMLReportGenerator;
import java.io.FileNotFoundException;
import java.io.IOException;

/**
 *
 * @author dnamorim
 */
public class RelatorioExperienciaController {
    
    private HTMLReportGenerator htmlrep;
    private FibraOptica fo;
    
    public RelatorioExperienciaController(FibraOptica fo) {
        this.fo = fo;
        htmlrep = new HTMLReportGenerator(fo);
    }

    public void setFicheiroAImportar(String file) {
        htmlrep.setFileToImport(file);
    }

    public void gerarRelatorio() throws FileNotFoundException {
        htmlrep.generateFile();
    }

    public boolean validaRelatorio() {
        return true;
    }
}
