/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fibraoptica.controller;

import fibraoptica.model.AppData;
import fibraoptica.model.Bainha;
import fibraoptica.model.MeioAmbiente;
import fibraoptica.model.Nucleo;
import fibraoptica.ui.MainUI;

/**
 *
 * @author dnamorim
 */
public class AdicionarValorReferenciaController {
    
    private AppData appdata;
    private String group;
    private Object refvalue;
    
    public AdicionarValorReferenciaController(AppData ad, String group) {
        this.appdata = ad;
        this.group = group;
    }
    
    public boolean createRefValue(String namePT, String nameEN, double index) {
        if (group.equalsIgnoreCase(MainUI.language.getString("nucleos"))) {
            refvalue = new Nucleo(namePT, nameEN, index);
        } else if (group.equalsIgnoreCase(MainUI.language.getString("bainhas"))) {
            refvalue = new Bainha(namePT, nameEN, index);
        } else if (group.equalsIgnoreCase(MainUI.language.getString("ambientes"))) {
            refvalue = new MeioAmbiente(namePT, nameEN, index);
        } else {
            return false;
        }
        return true;
    }
    
    public boolean validate() {
        if (group.equalsIgnoreCase(MainUI.language.getString("nucleos"))) {
            return ((Nucleo) refvalue).validate();
        } else if (group.equalsIgnoreCase(MainUI.language.getString("bainhas"))) {
            return ((Bainha) refvalue).validate();
        } else if (group.equalsIgnoreCase(MainUI.language.getString("ambientes"))) {
            return ((MeioAmbiente) refvalue).validate();
        }
        
        return true;
    }
    
    public boolean addRefValue() {
        if (group.equalsIgnoreCase(MainUI.language.getString("nucleos"))) {
            return this.appdata.addNucleo((Nucleo) refvalue);
        } else if (group.equalsIgnoreCase(MainUI.language.getString("bainhas"))) {
            return this.appdata.addBainha((Bainha) refvalue);
        } else if (group.equalsIgnoreCase(MainUI.language.getString("ambientes"))) {
            return this.appdata.addMeioAmbiente((MeioAmbiente) refvalue);
        }
        
        return false;
    }
}
