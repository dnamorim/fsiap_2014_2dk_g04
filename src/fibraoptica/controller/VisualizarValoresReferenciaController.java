/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fibraoptica.controller;

import fibraoptica.model.AppData;
import fibraoptica.model.Bainha;
import fibraoptica.model.MeioAmbiente;
import fibraoptica.model.Nucleo;
import fibraoptica.ui.MainUI;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import javax.swing.DefaultListModel;

/**
 *
 * @author dnamorim
 */
public class VisualizarValoresReferenciaController {
 
    private AppData appdata;
    private String groupValues;
    
    public VisualizarValoresReferenciaController(AppData ad, String group) {
       this.appdata = ad;
       this.groupValues = group;
    }
    
    public void deleteValues(List lst) {
        if (groupValues.equalsIgnoreCase(MainUI.language.getString("nucleos"))) {
            deleteNucleos(lst);
        } else if (groupValues.equalsIgnoreCase(MainUI.language.getString("bainhas"))) {
            deleteBainhas(lst);
        } else if (groupValues.equalsIgnoreCase(MainUI.language.getString("ambientes"))) {
            deleteMeiosAmbiente(lst);
        }
    }
    
    public void deleteAllValues() {
        if (groupValues.equalsIgnoreCase(MainUI.language.getString("nucleos"))) {
            deleteAllNucleos();
        } else if (groupValues.equalsIgnoreCase(MainUI.language.getString("bainhas"))) {
            deleteAllBainhas();
        } else if (groupValues.equalsIgnoreCase(MainUI.language.getString("ambientes"))) {
            deleteAllMeiosAmbiente();
        }
    }
    
    private void deleteNucleos(List<Item<Nucleo>> lst) {
        for (Item<Nucleo> n : lst) {
            this.appdata.removeNucleo(n.getItem());
        }
    }
    
    private void deleteBainhas(List<Item<Bainha>> lst) {
        for (Item<Bainha> b : lst) {
            this.appdata.removeBainha(b.getItem());
        }
    }
    
    private void deleteMeiosAmbiente(List<Item<MeioAmbiente>> lst) {
        for (Item<MeioAmbiente> ma : lst) {
            this.appdata.removeMeioAmbiente(ma.getItem());
        }
    }
    
    public Object[] getValues() {
        
        if (groupValues.equalsIgnoreCase(MainUI.language.getString("nucleos"))) {
            return getNucleos().toArray();
        } else if (groupValues.equalsIgnoreCase(MainUI.language.getString("bainhas"))) {
            return getBainhas().toArray();
        } else if (groupValues.equalsIgnoreCase(MainUI.language.getString("ambientes"))) {
            return getMeiosAmbiente().toArray();
        }
        
        return null;
    } 
    
    private List<Item<Nucleo>> getNucleos() {
        List<Item<Nucleo>> lst = new ArrayList();
        for (Nucleo n : this.appdata.getListNucleos()) {
            lst.add(new Item<>(n));
        }
        
        return lst;
    }
    
    private List<Item<Bainha>> getBainhas() {
        List<Item<Bainha>> lst = new ArrayList();
        for (Bainha b : this.appdata.getListBainhas()) {
            lst.add(new Item<>(b));
        }
        
        return lst;
    }
    
    private List<Item<MeioAmbiente>> getMeiosAmbiente() {
        List<Item<MeioAmbiente>> lst = new ArrayList();
        for (MeioAmbiente ma : this.appdata.getListMeiosAmbiente()) {
            lst.add(new Item<>(ma));
        }
        
        return lst;
    }

    private void deleteAllNucleos() {
        this.appdata.removeNucleo();
    }

    private void deleteAllBainhas() {
         this.appdata.removeBainha();
    }

    private void deleteAllMeiosAmbiente() {
        this.appdata.removeMeioAmbiente();
    }
    
    public class Item<E> {

        private E it;

        public Item(E e) {
            this.it = e;
        }

        public Item() {
            this.it = null;
        }

        public E getItem() {
            return it;
        }

        public void setItem(E e) {
            this.it = e;
        }

        @Override
        public String toString() {
            if (it instanceof Nucleo) {
                Nucleo n = (Nucleo) it;
                 
                if(MainUI.language.getLocale().equals(Locale.UK)) {
                   return String.format("%s (%.3f)", n.getNomeEng(), n.getIndex());
                } else {
                    return String.format("%s (%.3f)", n.getNomePT(), n.getIndex());
                }
            }

            if (it instanceof Bainha) {
                Bainha b = (Bainha) it;
                if(MainUI.language.getLocale().equals(Locale.UK)) {
                   return String.format("%s (%.3f)", b.getNomeEng(), b.getIndex());
                } else {
                    return String.format("%s (%.3f)", b.getNomePT(), b.getIndex());
                }
                
            }

            if (it instanceof MeioAmbiente) {
                MeioAmbiente a = (MeioAmbiente) it;
                if(MainUI.language.getLocale().equals(Locale.UK)) {
                   return String.format("%s (%.3f)", a.getNomeEng(), a.getIndex());
                } else {
                    return String.format("%s (%.3f)", a.getNomePT(), a.getIndex());
                }
                
            }

            return it.toString();
        }
    }

}
