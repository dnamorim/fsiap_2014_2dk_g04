/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fibraoptica.model;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.*;

/**
 * Class AppData
 * @author dnamorim
 */
public class AppData implements Serializable {
    private List<Nucleo> lstNucleos;
    private List<Bainha> lstBainhas;
    private List<MeioAmbiente> lstMeiosAmbiente;
    
    public AppData() {
        this.lstNucleos = new ArrayList<>();
        this.lstBainhas = new ArrayList<>();
        this.lstMeiosAmbiente = new ArrayList<>();
    }
    
    public boolean addNucleo(Nucleo n) {
        if(!existsNucleo(n) && n.validate()) {
            return this.lstNucleos.add(n);
        }
        return false;
    }
    
    public boolean addBainha(Bainha b) {
        if(!existsBainha(b) && b.validate()) {
            return this.lstBainhas.add(b);
        }
        return false;
    }
    
    public boolean addMeioAmbiente(MeioAmbiente ma) {
        if(!existsMeioAmbiente(ma) && ma.validate()) {
            return this.lstMeiosAmbiente.add(ma);
        }
        return false;
    }
    
    public List<Nucleo> getListNucleos() {
        return this.lstNucleos;
    }
    
    public List<Bainha> getListBainhas() {
        return this.lstBainhas;
    }
    
    public List<MeioAmbiente> getListMeiosAmbiente() {
        return this.lstMeiosAmbiente;
    }
    
    public boolean isEmpty() {
        return (this.lstNucleos.isEmpty() && this.lstBainhas.isEmpty() && this.lstMeiosAmbiente.isEmpty());
    }
    
    public boolean existsNucleo(Nucleo n) {
        return this.lstNucleos.contains(n);
    }
    
    public boolean existsBainha(Bainha b) {
       return this.lstBainhas.contains(b);
    }
    
    public boolean existsMeioAmbiente(MeioAmbiente ma) {
       return this.lstMeiosAmbiente.contains(ma);
    }
    
    public boolean removeNucleo(Nucleo n) {
        return this.lstNucleos.remove(n);
    }
    
    public boolean removeBainha(Bainha b) {
        return this.lstBainhas.remove(b);
    }
    
    public boolean removeMeioAmbiente(MeioAmbiente ma) {
        return this.lstMeiosAmbiente.remove(ma);
    }
    
    public boolean removeNucleo() {
        this.lstNucleos.clear();
        return true;
    }
    
    public boolean removeBainha() {
        this.lstBainhas.clear();
        return true;
    }
    
    public boolean removeMeioAmbiente() {
        this.lstMeiosAmbiente.clear();
        return true;
    }
    
    public void saveData() throws IOException {
        ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream("data.bin"));
        out.writeObject(this.lstNucleos);
        out.writeObject(this.lstBainhas);
        out.writeObject(this.lstMeiosAmbiente);
        out.close();
    }
    
    public void loadData() throws IOException, ClassNotFoundException {
        ObjectInputStream in = new ObjectInputStream(new FileInputStream("data.bin"));
        this.lstNucleos = (List<Nucleo>) in.readObject();
        this.lstBainhas = (List<Bainha>) in.readObject();
        this.lstMeiosAmbiente = (List<MeioAmbiente>) in.readObject();
        in.close();
    }
    
}
