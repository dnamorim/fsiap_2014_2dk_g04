/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fibraoptica.model;

import static fibraoptica.ui.MainUI.language;

/**
 *
 * @author dnamorim
 */
public class BainhaInvalidaException extends IllegalArgumentException {
    
    public BainhaInvalidaException() {
        super(language.getString("bainhaInvalida"));
    }
    
    public BainhaInvalidaException(String mensagem) {
        super(mensagem);
    }
}
