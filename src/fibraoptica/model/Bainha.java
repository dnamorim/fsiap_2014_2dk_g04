/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fibraoptica.model;

import static fibraoptica.ui.MainUI.language;
import java.io.Serializable;

/**
 * Class Bainha
 * @author Sara Freitas
 */
public class Bainha implements Serializable {
    /**
     * Designação da Bainha (em Português) 
     */
    private String nomePT;
    
    /**
     * Designação da Bainha (in English)
     */
    private String nomeEng;
    
    /**
     * Índice de Refracção
     */
    private double index;
    
    /**
     * Construtor da Bainha com parâmetros
     * @param nomePT nome em Português
     * @param nomeEng Nome em Inglês
     * @param index Valor do Índice de Refracção
     */
    public Bainha(String nomePT, String nomeEng, double index) {
        this.nomePT = nomePT;
        this.nomeEng = nomeEng;
        this.index = index;
    }
    
    /**
     * Construtor Cópia da Bainha
     * @param other objecto bainha a copiar
     */
    public Bainha(Bainha other) {
        this(other.nomePT, other.nomeEng, other.index);
    }
    
    /**
     * Construtor por defeito da Bainha
     */
    public Bainha() {
        this("", "", 0f);
    }

    
    /**
     * Obtém a designação Portugesa da Bainha
     * @return nomePT
     */
    public String getNomePT() {
        return this.nomePT;
    }

    /**
     * Obtém a designação Inglesa da Bainha 
     * @return nomeEng
     */
    public String getNomeEng() {
        return this.nomeEng;
    }

    /**
     * Obtém o valor da índice de refracção da Bainha
     * @return the index
     */
    public double getIndex() {
        return this.index;
    }

    /**
     * Define uma nova designação em Português para a Bainha
     * @param nomePT novo nome em PT
     */
    public void setNomePT(String nomePT) {
        this.nomePT = nomePT;
    }

    /**
     * Define umaa nova designação em Inglês para a Bainha
     * @param nomeEng novo nome em ENG
     */
    public void setNomeEng(String nomeEng) {
        this.nomeEng = nomeEng;
    }

    /**
     * Define uma novo valor da Índice de Refracção para a Bainha
     * @param index novo índice de refracção
     */
    public void setIndex(double index) {
        this.index = index;
    }

    /**
     * Valida a Bainha
     * @return true se estiver bem construida, senão false
     */
    public boolean validate() {
        if (!this.nomePT.isEmpty() && !this.nomeEng.isEmpty() && this.index>=1) {
            return true;
        }
        
        return false;
    }
    
    
    /**
     * Devolve umaa descrição textual da Bainha 
     * @return desingação da Bainha
     */
    @Override
    public String toString() {
        return String.format(language.getString("bainha") + " %s / %s. "+ language.getString("indiceRefracao") +" %.4f" , this.nomePT, this.nomeEng, this.index);
    }

     /**
     * Compara uma Bainha com outro Objecto recebido por parâmetro
     * @param obj objecto a ser comparado
     * @return true se forem a mesma bainha. Caso Contrário, false.
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        
        if (obj == null || this.getClass() != obj.getClass()) {
            return false;
        }
        
        Bainha n = (Bainha) obj;
        return (this.nomePT.equalsIgnoreCase(n.getNomePT()) && this.nomeEng.equalsIgnoreCase(n.getNomeEng()) && this.index == n.getIndex());
    }
    
}
