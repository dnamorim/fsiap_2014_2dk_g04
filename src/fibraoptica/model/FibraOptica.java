/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fibraoptica.model;

import fibraoptica.ui.MainUI;
import java.util.Locale;

/**
 *
 * @author Utilizador
 */
public class FibraOptica {

    /**
     * O Núcleo da Fibra Óptica
     */
    private Nucleo nucleo;
    
    /**
     * A Bainha da Fibra Óptica
     */
    private Bainha bainha;
    
    /**
     * O Meio Ambiente onde se Insere a Fibra Óptica
     */
    private MeioAmbiente meioAmbiente;
    
    /**
     * Ângulo de Incidência da luz na Fibra Óptica
     */
    private double angIncidencia;
    
    /**
     * Ângulo Crítico da Fibra Óptica (Calculado)
     */
    private double angCritico;
    
    /**
     * Ângulo de Aceitação da Fibra Óptica (Calculado)
     */
    private double angAceitacao;
    
    /**
     * Abertura Numérica da Fibra Óptica (Calculado)
     */
    private double aberturaNumerica;
    
    /**
     * Ângulo Refractado na Fibra Óptica (Calculado)
     */
    private double angRefractado;    

    /**
     * Cria uma Instância de uma Fibra Óptica
     * @param nucleo Núcleo da Fibra Óptica
     * @param bainha Bainha da Fibra Óptica
     * @param meioAmbiente Meio Ambiente da Fibra Óptica
     */
    public FibraOptica(Nucleo nucleo, Bainha bainha, MeioAmbiente meioAmbiente) {
        this.nucleo = nucleo;
        setBainha(bainha);
        this.meioAmbiente = meioAmbiente;
        
        //  Inicialização de Variáveis
        this.angCritico = 0;
        this.angAceitacao = 0;
        this.aberturaNumerica = 0;
        this.angRefractado = 0;
    }
    
    /**
     * Cria um clone de uma Fibra Óptica
     * @param fo Fibra Óptica a Copiar
     */
    public FibraOptica(FibraOptica fo) {
        this(fo.nucleo, fo.bainha, fo.meioAmbiente);
    }
    
    /**
     * Cria uma Instância Padrão de Fibra Óptica
     */
    public FibraOptica() {
        this(null, null, null);
    }

    /**
     * Define o ângulo de incidência da luz na fibra óptica
     * @param angIncidencia ângulo de incidência
     */
    public void setAngIncidencia(double angIncidencia) {
        this.angIncidencia = angIncidencia;
    }

    /**
     * Define o Núcleo da Fibra Óptica
     * @param n núcleo da fibra óptica
     */
    public void setNucleo(Nucleo n) {
        this.nucleo = n;
    }
    
    /**
     * Define a Bainha da Fibra Óptica
     * @param b bainha da fibra óptica
     */
    public void setBainha(Bainha b) {
        if(this.nucleo != null) {
            if(this.nucleo.getIndex() > b.getIndex()) {
                this.bainha = b;
            } else {
               String[] str = {"Não é possível construir a Fibra Óptica. O Índice de Refracção da Bainha é superior ao do Núcleo.", 
                            "Impossibble to build the Optic Fiber. Cladding Index is far superior than the core index."};
                if(MainUI.language.getLocale().equals(Locale.UK)) {
                    throw new BainhaInvalidaException(str[1]); // Em Português 
                } else {
                    throw new BainhaInvalidaException(str[0]); // Em Português 
                }
            }
        } else {
            String[] str = {"Não é possível construir a Fibra Óptica. Necessário fornecer um Núcleo válido.", 
                            "Impossibble to build the Optic Fiber. Please, provide a valid core."};
                if(MainUI.language.getLocale().equals(Locale.UK)) {
                    throw new BainhaInvalidaException(str[1]); // Em Português 
                } else {
                    throw new BainhaInvalidaException(str[0]); // Em Português 
                }
        }
    }
    
     /**
     * Define o MeioAmbiente onde está inserida a Fibra Óptica
     * @param ma meio ambiente da fibra óptica
     */
    public void setMeioAmbiente(MeioAmbiente ma) {
        this.meioAmbiente = ma;
    }
    
    /**
     * Devolve o Núcleo da Fibra Óptica
     * @return núcleo da fibra
     */
    public Nucleo getNucleo() {
        return this.nucleo;
    }
    
    /**
     * Devolve a Bainha da Fibra Óptica
     * @return bainha da fibra
     */
    public Bainha getBainha() {
        return this.bainha;
    }
    
    /**
     * Devolve o Meio Ambiente onde está inserida a fibra óptica
     * @return meio ambiente da fibra óptica
     */
    public MeioAmbiente getMeioAmbiente() {
        return this.meioAmbiente;
    }
    
    /**
     * Devolve o ângulo de incidência da luz na fibra óptica
     * @return angulo de incidencia
     */
    public double getAngIncidencia() {
        return this.angIncidencia;
    }
    
    /**
     * Devolve o Ângulo Crítico calculado
     * @return ângulo crítico
     */
    public double getAngCritico() {
        return angCritico;
    }

    /**
     * Devolve o Ângulo de Aceitação Calculado
     * @return Ângulo de aceitação
     */
    public double getAngAceitacao() {
        return angAceitacao;
    }
    
    /**
     * Devolve a Abertura Numérica Calculada
     * @return abertura numérica
     */
    public double getAberturaNumerica() {
        return this.aberturaNumerica;
    }
    
    /**
     * Devolve o Ângulo Refractado Calculado
     * @return angulo refractado
     */
    public double getAngRefractado() {
        return this.angRefractado;
    }
    
     /**
     * Devolve o Cone de Aceitação de uma Fibra Óptica
     * @return cone de aceitacao
     */
    public double getConeAceitacao() {
        return 2*this.angAceitacao;
    }
    
    /**
     * Calcula os Valores dos ângulos e abertura numérica da fibra após construção com sucesso da Fibra óptica
     */
    public void efetuarCalculos() {
        this.angCritico = this.calcAngCritico();
        this.angAceitacao = this.calcAngAceitacao();
        this.aberturaNumerica = this.calcAberturaNumerica();
        this.angRefractado = this.calcAngRefractadoTP();
    }
    
    /**
     * Indica, para um dado Ângulo de Incidência, se existe ou não Reflexão Total dentro da fibra
     * @return true se existe reflexão total/ false se não existe
     */
    public boolean hasReflexaoTotal() {
        if(this.angIncidencia < this.angAceitacao) {
            return true;
        }
        return false;
    }
    
     /**
     * Devolve o Ângulo Refractado na Entrada da Fibra Óptica via Lei Snell
     * 
     * Fórmula: angref = arcsen((indexMeio * sen(angIncidencia)/indexNucleo)
     * @return 
     */
    public double calcAngRefEnt() {
        return leiSnellAngle(this.getMeioAmbiente().getIndex(), this.angIncidencia, this.getNucleo().getIndex());
    }
    
    /**
     * Devolve o ângulo Reflectido segundo o ângulo de incidência dado
     * 
     * @return angulo reflectido
     */
    public double calcAngRefIncidente() {
        return leiSnellAngle(this.getNucleo().getIndex(), 90 - this.calcAngRefEnt(), this.getBainha().getIndex());
    }
    
    
    /**
     * 
     * @param nA Índice Refracção Meio A
     * @param angA Ângulo incidente no meio A
     * @param nB Índice Refracção Meio B
     * @return ângulo refractado no meio B
     */
    private static double leiSnellAngle(double nA, double angA, double nB) {
        return Math.toDegrees(Math.asin((nA * Math.sin(Math.toRadians(angA)))/ nB));
    }
    
    /**
     * Calcula o Ângulo Crítico da Fibra Óptica
     * 
     * acritico = arcsen(indexBainha / indexNucleo)
     * Fórmula in: http://www.ipb.pt/~fmoreira/Ensino/Ondas/Ondas_04_05_cap5.pdf
     * @return angulo critico
     */
    private double calcAngCritico() {
        return Math.toDegrees(Math.asin(this.bainha.getIndex() / this.nucleo.getIndex()));
    }
    
    /**
     * Calcula o Ângulo de Aceitação da Fibra Óptica
     * 
     * aaceitacao = arcsen(sqrt(indexNucleo^2 - indexBainha^2) / indexMeioAmbiente)
     * Fórmula in: http://www.ipb.pt/~fmoreira/Ensino/Ondas/Ondas_04_05_cap5.pdf
     * @return angulo aceitacao
     */
    private double calcAngAceitacao() {
        return Math.toDegrees(Math.asin(Math.sqrt(Math.pow(this.nucleo.getIndex(), 2) - Math.pow(this.bainha.getIndex(), 2)) / this.meioAmbiente.getIndex()));
    }
    
    /**
     * Calcula a Abertura Numérica da Fibra Óptica
     * 
     * Fórmula in: http://www.ipb.pt/~fmoreira/Ensino/Ondas/Ondas_04_05_cap5.pdf
     * @return abertura numerica
     */
    private double calcAberturaNumerica() {
        return Math.sqrt(Math.pow(this.nucleo.getIndex(), 2) - Math.pow(this.bainha.getIndex(), 2));
    }
    
    /**
     * Calcula o Ângulo Refractado da Fibra Óptica via Teorema de Pitágoras
     * 
     * Através da relação via Teorema de Pitágoras entre o ângulo refractado e o ângulo crítico da Fibra Óptica,
     * obtém-se o Ângulo Refractado via: 90º - Ang.Critico
     * 
     * @return angulo refractado
     */
    private double calcAngRefractadoTP() {
        return (90 - this.calcAngCritico());
    }
    

   
}
