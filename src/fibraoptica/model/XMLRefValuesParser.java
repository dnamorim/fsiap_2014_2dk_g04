/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fibraoptica.model;

import java.io.File;
import java.io.IOException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 * Adaptado a partir de: http://www.mkyong.com/java/how-to-read-xml-file-in-java-dom-parser/
 * @author dnamorim
 */
public class XMLRefValuesParser {
    
    private File fileXML;
    private AppData appdata;
    private DocumentBuilderFactory dbFactory;
    private DocumentBuilder dBuilder;
    private Document doc;    
            
    public  XMLRefValuesParser(AppData appdata, String pathFile) throws ParserConfigurationException, SAXException, IOException {
        fileXML = new File(pathFile);
        this.appdata = appdata;
        dbFactory = DocumentBuilderFactory.newInstance();
        dBuilder = dbFactory.newDocumentBuilder();
        doc = dBuilder.parse(fileXML);
        doc.getDocumentElement().normalize();
    }
    
    public void importCores() {
        NodeList nList = doc.getElementsByTagName("core");
        Nucleo n = new Nucleo();
        
        for (int i = 0; i < nList.getLength(); i++) {
            Node node = nList.item(i);
            
            if (node.getNodeType() == Node.ELEMENT_NODE) {
                Element elm = (Element) node;
                n.setNomePT(elm.getElementsByTagName("pt").item(0).getTextContent());
                n.setNomeEng(elm.getElementsByTagName("en").item(0).getTextContent());
                n.setIndex(Double.parseDouble(elm.getElementsByTagName("index").item(0).getTextContent()));
                this.appdata.addNucleo(new Nucleo(n));
            }
        }
    }
    
    public void importCladdings() {
        NodeList nList = doc.getElementsByTagName("cladding");
        Bainha b = new Bainha();
        
        for (int i = 0; i < nList.getLength(); i++) {
            Node node = nList.item(i);
            
            if (node.getNodeType() == Node.ELEMENT_NODE) {
                Element elm = (Element) node;
                b.setNomePT(elm.getElementsByTagName("pt").item(0).getTextContent());
                b.setNomeEng(elm.getElementsByTagName("en").item(0).getTextContent());
                b.setIndex(Double.parseDouble(elm.getElementsByTagName("index").item(0).getTextContent()));
                this.appdata.addBainha(new Bainha(b));
            }
        }
    }
    
    public void importEnvironments() {
        NodeList nList = doc.getElementsByTagName("environment");
        MeioAmbiente ma = new MeioAmbiente();
        
        for (int i = 0; i < nList.getLength(); i++) {
            Node node = nList.item(i);
            
            if (node.getNodeType() == Node.ELEMENT_NODE) {
                Element elm = (Element) node;
                ma.setNomePT(elm.getElementsByTagName("pt").item(0).getTextContent());
                ma.setNomeEng(elm.getElementsByTagName("en").item(0).getTextContent());
                ma.setIndex(Double.parseDouble(elm.getElementsByTagName("index").item(0).getTextContent()));
                this.appdata.addMeioAmbiente(new MeioAmbiente(ma));
            }
        }
    }
    
}
