/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fibraoptica.model;

import static fibraoptica.ui.MainUI.language;
import java.io.Serializable;

/**
 * Class Núcleo
 * @author dnamorim
 */
public class Nucleo implements Serializable {
    /**
     * Designação do Núcleo (em Português) 
     */
    private String nomePT;
    
    /**
     * Designação do Núcleo (in English)
     */
    private String nomeEng;
    
    /**
     * Índice de Refracção
     */
    private double index;
    
    /**
     * Construtor de Núcleo com parâmetros
     * @param nomePT nome em Português
     * @param nomeEng Nome em Inglês
     * @param index Valor do Índice de Refracção
     */
    public Nucleo(String nomePT, String nomeEng, double index) {
        this.nomePT = nomePT;
        this.nomeEng = nomeEng;
        this.index = index;
    }
    
    /**
     * Construtor Cópia de Núcleo
     * @param other objecto núcleo a copiar
     */
    public Nucleo(Nucleo other) {
        this(other.nomePT, other.nomeEng, other.index);
    }
    
    /**
     * Construtor por defeito do Núcleo
     */
    public Nucleo() {
        this("", "", 0f);
    }

    
    /**
     * Obtém a designação Portugesa do Núcleo
     * @return nomePT
     */
    public String getNomePT() {
        return this.nomePT;
    }

    /**
     * Obtém a designação Inglesa do Núcleo 
     * @return nomeEng
     */
    public String getNomeEng() {
        return this.nomeEng;
    }

    /**
     * Obtém o valor do índice de refracção do Núcleo
     * @return the index
     */
    public double getIndex() {
        return this.index;
    }

    /**
     * Define uma nova designação em Português para o Núcleo
     * @param nomePT novo nome em PT
     */
    public void setNomePT(String nomePT) {
        this.nomePT = nomePT;
    }

    /**
     * Define uma nova designação em Inglês para o Núcleo
     * @param nomeEng novo nome em ENG
     */
    public void setNomeEng(String nomeEng) {
        this.nomeEng = nomeEng;
    }

    /**
     * Define um novo valor do Índice de Refracção para o Núcleo
     * @param index novo índice de refracção
     */
    public void setIndex(double index) {
        this.index = index;
    }

    /**
     * Valida o Núcleo
     * @return true se estiver bem construido, senão false
     */
    public boolean validate() {
        if (!this.nomePT.isEmpty() && !this.nomeEng.isEmpty() && this.index>=1) {
            return true;
        }
        
        return false;
    }
    
    
    /**
     * Devolve uma descrição textual do Núcleo 
     * @return desingação do Núcleo
     */
    @Override
    public String toString() {
        return String.format(language.getString("nucleo") + " %s / %s. "+ language.getString("indiceRefracao") +" %.4f" , this.nomePT, this.nomeEng, this.index);
    }

     /**
     * Compara um Núcleo com outro Objecto recebido por parâmetro
     * @param obj objecto a ser comparado
     * @return true se forem o mesmo núcleo. Caso Contrário, false.
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        
        if (obj == null || this.getClass() != obj.getClass()) {
            return false;
        }
        
        Nucleo n = (Nucleo) obj;
        return (this.nomePT.equalsIgnoreCase(n.getNomePT()) && this.nomeEng.equalsIgnoreCase(n.getNomeEng()) && this.index == n.getIndex());
    }
    
}
