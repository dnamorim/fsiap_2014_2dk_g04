/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fibraoptica.model;

import com.hp.gagawa.java.Document;
import com.hp.gagawa.java.DocumentType;
import com.hp.gagawa.java.elements.*;
import fibraoptica.ui.MainUI;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 *
 * @author dnamorim
 */
public class HTMLReportGenerator {
    
    private String path;
    private FibraOptica fo;
    private Document html;
    
    public HTMLReportGenerator(FibraOptica fo) {
        this.fo = fo;
        this.html = new Document(DocumentType.XHTMLStrict);
    }
    
    public void setFileToImport(String file) {
        this.path = file;
    }
    
    private void createHTML() {
        createHeader();
        createOpticFiberInfo();
        createOpticFiberSpecifications();
        createResultExperience();
        createTime();
    }
    
    private void createHeader() {
        html.head.appendChild(new Title().appendChild(new Text(MainUI.language.getString("htmlTitle")) ) );
        html.head.appendChild(new Meta("text/html; charset=UTF-8").setHttpEquiv("Content-Type"));
        html.body.appendChild(new H1().appendChild(new Text(MainUI.language.getString("htmlTitle")) ));
    }
    
    private void createOpticFiberInfo() {
        html.body.appendChild(new H3().appendChild(new Text(MainUI.language.getString("htmlFibraOptica"))));
        
        //  p nucleo
        P pNucleo = new P();
        String nucleo = String.format("<b>%s: </b> %s  -  <b>%s: </b> %.3f", MainUI.language.getString("lblNucleo"), (MainUI.language.getLocale().equals(Locale.UK)) ? this.fo.getNucleo().getNomeEng() : this.fo.getNucleo().getNomePT() ,
                                        MainUI.language.getString("indiceRefracao"), this.fo.getNucleo().getIndex());
        pNucleo.appendChild(new Text(nucleo));
        html.body.appendChild(pNucleo);
        
        //  p bainha
        P pBainha = new P();
        String bainha = String.format("<b>%s: </b> %s  -  <b>%s: </b> %.3f", MainUI.language.getString("lblBainha"), 
                (MainUI.language.getLocale().equals(Locale.UK)) ? this.fo.getBainha().getNomeEng() : this.fo.getBainha().getNomePT(),
                MainUI.language.getString("indiceRefracao"), this.fo.getBainha().getIndex());
        pBainha.appendChild(new Text(bainha));
        html.body.appendChild(pBainha);
        
        //  p meio
        P pMeio = new P();
        String meio = String.format("<b>%s: </b> %s  -  <b>%s: </b> %.3f", MainUI.language.getString("lblMeio"), 
                (MainUI.language.getLocale().equals(Locale.UK)) ? this.fo.getMeioAmbiente().getNomeEng() : this.fo.getMeioAmbiente().getNomePT(),
                MainUI.language.getString("indiceRefracao"), this.fo.getMeioAmbiente().getIndex());
        pMeio.appendChild(new Text(meio));
        html.body.appendChild(pMeio);
        
        html.body.appendChild(new P().appendChild(new Text(MainUI.language.getString("htmlFibraIndices"))));
        
    }
    
    private void createOpticFiberSpecifications() {
        html.body.appendChild(new H3().appendChild(new Text(MainUI.language.getString("htmlSpecsFibra"))));
        
        //  p ang critico
        P pCritico = new P();
        String critico = String.format("<b>%s: </b> %.3fº", MainUI.language.getString("lblAngCritico"), this.fo.getAngCritico());
        pCritico.appendChild(new Text(critico));
        html.body.appendChild(pCritico);
       
        //  p abetura numerica
        P pAN = new P();
        String an = String.format("<b>%s: </b> %.3f", MainUI.language.getString("lblAbertura"), this.fo.getAberturaNumerica());
        pAN.appendChild(new Text(an));
        html.body.appendChild(pAN);
        
        //  p ang aceitacao
        P pAceitacao = new P();
        String aceitacao = String.format("<b>%s: </b> %.3f", MainUI.language.getString("lblAngAceitacao"), this.fo.getAngAceitacao());
        pAceitacao.appendChild(new Text(aceitacao));
        html.body.appendChild(pAceitacao);
        
        //  p cone aceitacao
        P pCone = new P();
        String cone = String.format("<b>%s: </b> %.3f º", MainUI.language.getString("lblConeAceitacao"), this.fo.getConeAceitacao());
        pCone.appendChild(new Text(cone));
        html.body.appendChild(pCone);
        
        //  p ang refractado
        P pRefractado = new P();
        String refractado = String.format("<b>%s: </b> %.3f º", MainUI.language.getString("htmlAngRefEntFO"), this.fo.getAngRefractado());
        pRefractado.appendChild(new Text(refractado));
        html.body.appendChild(pRefractado);
        
        //  p ang incidencia
        P pIncidencia = new P();
        String incidencia = String.format("<b>%s: </b> %.3f º", MainUI.language.getString("htmlAngIncidencia"), this.fo.getAngIncidencia());
        pIncidencia.appendChild(new Text(incidencia));
        html.body.appendChild(pIncidencia);
        
        //  p ang incidencia
        P pRefractadoInc = new P();
        String refractadoInc = String.format("<b>%s: </b> %.3f º", MainUI.language.getString("htmlAngRefractado"), this.fo.calcAngRefEnt());
        pRefractadoInc.appendChild(new Text(refractadoInc));
        html.body.appendChild(pRefractadoInc);
        
        //  p ang reflectido
        P pReflectido = new P();
        double ang = this.fo.calcAngRefIncidente();
        String reflectido = String.format("<b>%s: </b> %s º", MainUI.language.getString("htmlAngReflectido"), (Double.isNaN(ang)) ? "---" : String.format("%.3f", ang));
        pReflectido.appendChild(new Text(reflectido));
        html.body.appendChild(pReflectido);
        
    }
    
    private void createResultExperience() {
        P pResult = new P();
        pResult.appendChild(new Text((this.fo.hasReflexaoTotal()) ? MainUI.language.getString("htmlResumeSuccess") : MainUI.language.getString("htmlResumeNot") ));
        html.body.appendChild(pResult);
    }
    
    private void createTime() {
        html.body.appendChild(new Hr());
        html.body.appendChild(new Text("</hr>"));
        html.body.appendChild(new Text("  <p>\n" +
"    <a href=\"http://validator.w3.org/check?uri=referer\"><img\n" +
"      src=\"http://www.w3.org/Icons/valid-xhtml10\" alt=\"Valid XHTML 1.0 Strict\" height=\"31\" width=\"88\" /></a>\n" +
"  </p>"));
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();
        html.body.appendChild(new Div().appendChild(new Text(dateFormat.format(date))));
    }
    
    public void generateFile() throws FileNotFoundException {
        createHTML();
        File fileHTML = new File(path);
        PrintWriter out = new PrintWriter(new FileOutputStream(fileHTML));
        out.println(html.write());
        out.close();
    }
}
