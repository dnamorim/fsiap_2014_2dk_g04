/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fibraoptica.model;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.List;

/**
 *
 * @author dnamorim
 */
public class XMLValuesWriter {
    private PrintWriter writer;
    
    public XMLValuesWriter(String path) throws FileNotFoundException {
        this.writer = new PrintWriter(path);
        writer.println("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
        writer.println("<list>");
    }
    
    private void writeCores(List<Nucleo> lst) {
            for (Nucleo n : lst) {
                writer.println("\t\t<core>");
                writer.println("\t\t\t<name>");
                writer.println("\t\t\t\t<pt>" + n.getNomePT() + "</pt>");
                writer.println("\t\t\t\t<en>" + n.getNomeEng() + "</en>");
                writer.println("\t\t\t</name>");
                writer.println("\t\t\t<index>" + n.getIndex() + "</index>");
                writer.println("\t\t</core>");
            }
    }
    
    private void writeCladdings(List<Bainha> lst) {
             for (Bainha b : lst) {
                writer.println("\t\t<cladding>");
                writer.println("\t\t\t<name>");
                writer.println("\t\t\t\t<pt>" + b.getNomePT() + "</pt>");
                writer.println("\t\t\t\t<en>" + b.getNomeEng() + "</en>");
                writer.println("\t\t\t</name>");
                writer.println("\t\t\t<index>" + b.getIndex() + "</index>");
                writer.println("\t\t</cladding>");
            }
    }
    
    public void writeMaterials(List<Nucleo> lstn, List<Bainha> lstb) {
        if (lstn.size() > 0 || lstb.size() > 0) {
            writer.println("\t<materials>");
            this.writeCores(lstn);
            this.writeCladdings(lstb);
            writer.println("\t</materials>");
        }
    }
    
    public void writeEnvironments(List<MeioAmbiente> lstm) {
        if(lstm.size() > 0) {
            writer.println("\t<environments>");
            for (MeioAmbiente ma : lstm) {
                writer.println("\t\t<environment>");
                writer.println("\t\t\t<name>");
                writer.println("\t\t\t\t<pt>" + ma.getNomePT() + "</pt>");
                writer.println("\t\t\t\t<en>" + ma.getNomeEng() + "</en>");
                writer.println("\t\t\t</name>");
                writer.println("\t\t\t<index>" + ma.getIndex() + "</index>");
                writer.println("\t\t</environment>");
            }
            writer.println("\t</environments>");
        }
        
    }
    
    public void endWritter() {
        writer.println("</list>");
        writer.close();
    }
}
