/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fibraoptica.ui;

import fibraoptica.controller.ImportarValoresReferenciaController;
import fibraoptica.model.AppData;
import java.awt.Frame;
import java.io.IOException;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;

/**
 *
 * @author dnamorim
 */
public class ImportarValoresReferenciaUI {
    
    private ImportarValoresReferenciaController controllerIVR;
    
    public ImportarValoresReferenciaUI(AppData appdata, Frame father) {
        controllerIVR = new ImportarValoresReferenciaController(appdata);
        
        JFileChooser chDialog = new JFileChooser();
        chDialog.setDialogTitle(MainUI.language.getString("titleImport"));
        FileNameExtensionFilter filter = new FileNameExtensionFilter(MainUI.language.getString("filechooserXML"), "xml");
        chDialog.setFileFilter(filter);
        int returnVal = chDialog.showOpenDialog(father);
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            try {
                controllerIVR.setFicheiroAImportar(chDialog.getSelectedFile().getAbsolutePath());
                controllerIVR.importarValores();
                if (controllerIVR.validaImportacao()) {
                    JOptionPane.showMessageDialog(father, MainUI.language.getString("msgSucessoImport"), MainUI.language.getString("titleImport"), JOptionPane.INFORMATION_MESSAGE);
                } else {
                    JOptionPane.showMessageDialog(father, MainUI.language.getString("msgInsucessoImportar"), MainUI.language.getString("titleImport"), JOptionPane.WARNING_MESSAGE);
                }
            } catch (IOException ex) {
                JOptionPane.showMessageDialog(father, MainUI.language.getString("msgInvalidFileImport"), MainUI.language.getString("titleImport"), JOptionPane.WARNING_MESSAGE);
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(father, MainUI.language.getString("msgUnexpectedErrorImport"), MainUI.language.getString("titleImport"), JOptionPane.WARNING_MESSAGE);
            }
        }
    }
    
}
