/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fibraoptica.ui;

import fibraoptica.controller.RelatorioExperienciaController;
import fibraoptica.model.AppData;
import fibraoptica.controller.SimularExperienciaController;
import java.io.FileNotFoundException;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;

/**
 *
 * @author dnamorim
 */
public class RelatorioExperienciaUI {
    
    private RelatorioExperienciaController controllerRE;
    private JFrame parent;
    
    public RelatorioExperienciaUI(AppData ad, SimularExperienciaController cse) {
        controllerRE = new RelatorioExperienciaController(cse.getFibraOptica());
        
        String file = null;
        file = selectFile();
        
        if(file != null) {
            try {
                controllerRE.setFicheiroAImportar(file);
                controllerRE.gerarRelatorio();
                JOptionPane.showMessageDialog(parent, String.format("%s %s.", MainUI.language.getString("msgSucessoRelatorio"), file), MainUI.language.getString("titleRelatorio"), JOptionPane.INFORMATION_MESSAGE);
            } catch (FileNotFoundException ex) {
                JOptionPane.showMessageDialog(parent, MainUI.language.getString("msgInvalidFileRelatorio"), MainUI.language.getString("titleRelatorio"), JOptionPane.WARNING_MESSAGE);
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(parent, MainUI.language.getString("msgUnexpectedErrorRelatorio"), MainUI.language.getString("titleRelatorio"), JOptionPane.WARNING_MESSAGE);
            }
        
        }
        
    }
    
    private String selectFile() {
        JFileChooser chDialog = new JFileChooser();
        chDialog.setDialogTitle(MainUI.language.getString("msgSaveRelatorio"));
        FileNameExtensionFilter filter = new FileNameExtensionFilter(MainUI.language.getString("filechooserHTML"), "html");
        chDialog.setFileFilter(filter);
        int userSelection = chDialog.showSaveDialog(parent);
 
            if (userSelection == JFileChooser.APPROVE_OPTION) {
                String fileToSave = chDialog.getSelectedFile().getAbsolutePath();
            
                if (!fileToSave.endsWith(".html")) {
                    fileToSave += ".html";
                }
                return fileToSave;
            }
            
        return null;
    }
}
