/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fibraoptica.ui;

import fibraoptica.controller.ExportarValoresReferenciaController;
import fibraoptica.model.AppData;
import static fibraoptica.ui.MainUI.language;
import java.awt.Frame;
import java.io.FileNotFoundException;
import java.io.IOException;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;


/**
 *
 * @author Sara Freitas
 */
public class ExportarValoresReferenciaUI {
    
    private ExportarValoresReferenciaController controller;
    
    public ExportarValoresReferenciaUI(AppData appdata, Frame father) throws FileNotFoundException {
        controller = new ExportarValoresReferenciaController(appdata);
        boolean flag = false;
        int userSelection;
        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setDialogTitle(MainUI.language.getString("exportarTitulo"));   
        FileNameExtensionFilter filter = new FileNameExtensionFilter(MainUI.language.getString("filechooserXML"), "xml");
        fileChooser.setFileFilter(filter);
        
            userSelection = fileChooser.showSaveDialog(father);
 
            if (userSelection == JFileChooser.APPROVE_OPTION) {
                String fileToSave = fileChooser.getSelectedFile().getAbsolutePath();
            
                if (!fileToSave.endsWith(".xml")) {
                    fileToSave += ".xml";
                }
            
                flag = controller.startWriter(fileToSave);
                
                if (!flag) {
                    JOptionPane.showMessageDialog(father, String.format("%s %s", language.getString("exportarExceptionFNF"), fileToSave), language.getString("exportarTitulo"), JOptionPane.WARNING_MESSAGE);
                } else {
                    controller.writeValues();
                    controller.endWriter();
                    JOptionPane.showMessageDialog(father, String.format("%s %s.", language.getString("exportarInfo"), fileToSave), language.getString("exportarTitulo"), JOptionPane.INFORMATION_MESSAGE);
                }
            }
     }
}
    

