#** README ** #

Repositório Git do Grupo nº 04 da turma 2DK para o ara o desenvolvimento do trabalho prático das Aulas Práticas-Laboratoriais da unidade curricular Física Aplicada (FSIAP).

##** Tema: Transmissão de Fibras **##
Relacionamento da transmissão na fibra com a abertura numérica da fibra, para diferentes tipos de fibra.

### Membros do Grupo: ###

* [Duarte Amorim](https://bitbucket.org/1130674) ([1130674@isep.ipp.pt](mailto:1130674@isep.ipp.pt))
* [Mónica Correia](https://bitbucket.org/1120598MonicaCorreia) ([1120598@isep.ipp.pt](mailto:1120598@isep.ipp.pt))
* [Sara Freitas](https://bitbucket.org/1130489) ([1130489@isep.ipp.pt](mailto:1130489@isep.ipp.pt))

### Orientadores ###

* Física: Prof. Paulo Fernandes ([paf@isep.ipp.pt](mailto:paf@isep.ipp.pt))
* Informática: [Prof. F. Jorge Duarte](https://bitbucket.org/fjd) ([fjd@isep.ipp.pt](mailto:fjd@isep.ipp.pt))

© LEI-ISEP, 2014