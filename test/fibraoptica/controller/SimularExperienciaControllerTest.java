/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fibraoptica.controller;

import fibraoptica.controller.SimularExperienciaController.*;
import fibraoptica.model.*;
import fibraoptica.model.Nucleo;
import fibraoptica.model.Bainha;
import fibraoptica.model.MeioAmbiente;
import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author dnamorim
 */
public class SimularExperienciaControllerTest {
    
    public SimularExperienciaControllerTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getItemsNucleo method, of class SimularExperienciaController.
     */
    @Test
    public void testGetItemsNucleo() {
        System.out.println("getItemsNucleo");
        AppData ad = new AppData();
        Nucleo n = new Nucleo("Núcleo Padrão", "Standard Core", 1.425);
        
        ad.addNucleo(n);
        SimularExperienciaController instance = new SimularExperienciaController(ad);
        List<Item<Nucleo>> lstexpResult = new ArrayList();
        lstexpResult.add(instance.new Item<Nucleo>(n));
        
        Object[] expResult = lstexpResult.toArray();
        Object[] result = instance.getItemsNucleo();
        boolean flag=true;
        
        if(expResult.length == result.length) {
            for (int i = 0; i < result.length; i++) {
                if(!( ((Item<Nucleo>)expResult[i]).getItem().equals( ((Item<Nucleo>)result[i]).getItem() )) ) {
                    flag = false;
                    break;
                }

            }
        } else {
            flag = false;
        }
        
        assertTrue(flag);
        
    }

    /**
     * Test of getItemsBainha method, of class SimularExperienciaController.
     */
    @Test
    public void testGetItemsBainha() {
        System.out.println("getItemsBainha");
        AppData ad = new AppData();
        Bainha b = new Bainha("Bainha Padrão", "Standard Cladding", 1.537);
        ad.addBainha(b);
        
        SimularExperienciaController instance = new SimularExperienciaController(ad);
        List<Item<Bainha>> lstExpResult = new ArrayList();
        lstExpResult.add(instance.new Item<Bainha>(b));
        
        Object[] expResult = lstExpResult.toArray();
        Object[] result = instance.getItemsBainha();
        boolean flag=true;
        
        if(expResult.length == result.length) {
            for (int i = 0; i < result.length; i++) {
                if(!( ((Item<Bainha>)expResult[i]).getItem().equals( ((Item<Bainha>)result[i]).getItem() )) ) {
                    flag = false;
                    break;
                }

            }
        } else {
            flag = false;
        }
        
        assertTrue(flag);
    }

    /**
     * Test of getItemsMeioAmbiente method, of class SimularExperienciaController.
     */
    @Test
    public void testGetItemsMeioAmbiente() {
        System.out.println("getItemsMeioAmbiente");
        AppData ad = new AppData();
        
        MeioAmbiente ma = new MeioAmbiente("Ar", "Ar", 1);
        ad.addMeioAmbiente(ma);
        
        SimularExperienciaController instance = new SimularExperienciaController(ad);
        List<Item<MeioAmbiente>> lstexpResult = new ArrayList();
        lstexpResult.add(instance.new Item<MeioAmbiente>(ma));
        
        Object[] expResult = lstexpResult.toArray();
        Object[] result = instance.getItemsMeioAmbiente();
        boolean flag=true;
        
        if(expResult.length == result.length) {
            for (int i = 0; i < result.length; i++) {
                if(!( ((Item<MeioAmbiente>)expResult[i]).getItem().equals( ((Item<MeioAmbiente>)result[i]).getItem() )) ) {
                    flag = false;
                    break;
                }

            }
        } else {
            flag = false;
        }
        
        assertTrue(flag);
    }

    /**
     * Test of buildFiber method, of class SimularExperienciaController.
     */
    @Test
    public void testBuildFiber() {
        System.out.println("buildFiber");
        
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of hasRefValues method, of class SimularExperienciaController.
     */
    @Test
    public void testHasRefValuesTrue() {
        System.out.println("hasRefValues: true");
        AppData ad = new AppData();
        ad.addMeioAmbiente(new MeioAmbiente("Ar", "Ar", 1));
        SimularExperienciaController instance = new SimularExperienciaController(ad);
        assertTrue(instance.hasRefValues());
    }
    
    /**
     * Test of hasRefValues method, of class SimularExperienciaController.
     */
    @Test
    public void testHasRefValuesFalse() {
        System.out.println("hasRefValues: false");
        AppData ad = new AppData();
        SimularExperienciaController instance = new SimularExperienciaController(ad);
        assertFalse(instance.hasRefValues());
    }
    
    /**
     * Test of Item<E> class, in SimularExperienciaController.
     */
    
    /**
     * Test of get and setItem methods, of class Item<E>.
     */
    @Test
    public void testgetSetItem() {
        System.out.println("get & setItem (Item<E>)");
        Nucleo n = new Nucleo("Núcleo Padrão", "Standard Core", 1.425);
        
        // Via http://stackoverflow.com/questions/7619505/how-can-i-resolve-an-enclosing-instance-that-contains-x-y-is-required
        SimularExperienciaController controllerSE = new SimularExperienciaController(new AppData());
        SimularExperienciaController.Item<Nucleo> itn = controllerSE.new Item<Nucleo>(n);
        itn.setItem(n);
        
        assertEquals(n, itn.getItem());
        
    }
 
    /**
     * Test of toString method, of class Item<E>.
     */
    @Test
    public void testToStringItem() {
        System.out.println("toString (Item<E>)");
        Nucleo n = new Nucleo("Núcleo Padrão", "Standard Core", 1.425);
        
        // Via http://stackoverflow.com/questions/7619505/how-can-i-resolve-an-enclosing-instance-that-contains-x-y-is-required
        SimularExperienciaController controllerSE = new SimularExperienciaController(new AppData());
        SimularExperienciaController.Item<Nucleo> itn = controllerSE.new Item<Nucleo>(n);
        
        String expResult = String.format("%s (%.3f)", n.getNomePT(), n.getIndex());
        assertEquals(expResult, itn.toString());
    }

}
