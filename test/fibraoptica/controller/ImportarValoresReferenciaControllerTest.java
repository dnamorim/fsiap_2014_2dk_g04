/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fibraoptica.controller;

import fibraoptica.model.AppData;
import fibraoptica.model.Nucleo;
import fibraoptica.model.Bainha;
import fibraoptica.model.MeioAmbiente;
import java.io.IOException;
import javax.xml.parsers.ParserConfigurationException;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.xml.sax.SAXException;

/**
 *
 * @author dnamorim
 */
public class ImportarValoresReferenciaControllerTest {
    
    public ImportarValoresReferenciaControllerTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }


    /**
     * Test of validaImportacao method, of class ImportarValoresReferenciaController.
     */
    @Test
    public void testGeralImportacao() throws ParserConfigurationException, SAXException, IOException {
        System.out.println("test Geral da Importacao");
        AppData ad = new AppData();
        
        ImportarValoresReferenciaController instance = new ImportarValoresReferenciaController(ad);
        instance.setFicheiroAImportar("xmlParserTestFile.xml");
        instance.importarValores();
        
        assertEquals(true, instance.validaImportacao());
    }
    
}
