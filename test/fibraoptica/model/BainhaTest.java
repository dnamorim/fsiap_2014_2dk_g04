/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fibraoptica.model;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Sara Freitas
 */
public class BainhaTest {
    
    public BainhaTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getNomePT method, of class Bainha.
     */
    @Test
    public void testGetSetNomePT() {
        System.out.println("get e Set NomePT");
        Bainha instance = new Bainha();
        String value = "Standard Cladding";
        instance.setNomePT(value);
        
        String result = instance.getNomePT();
        assertEquals(value, result);
    }

    /**
     * Test of getNomeEng method, of class Bainha.
     */
    @Test
    public void testGetSetNomeEng() {
        System.out.println("get e Set NomeEng");
        Bainha instance = new Bainha();
        String value = "Standard Cladding";
        instance.setNomeEng(value);
        
        String result = instance.getNomeEng();
        assertEquals(value, result);
    }

    /**
     * Test of getIndex method, of class Bainha.
     */
    @Test
    public void testGetIndex() {
        System.out.println("get e SetIndex");
        Bainha instance = new Bainha();
        double value = 1.25;
        instance.setIndex(value);
        double result = instance.getIndex();
        assertEquals(value, result, 0.000);
    }

    /**
     * Test of toString method, of class Bainha.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        Bainha instance = new Bainha("Bainha Padrão", "Standard Cladding", 1.25);
        String expResult = String.format("Bainha: %s (%s). Índice Refracção: %.4f" , instance.getNomePT(), instance.getNomeEng(), instance.getIndex() );
        
        String result = instance.toString();
        assertEquals(expResult, result);
    }

    /**
     * Test of equals method, of class Bainha.
     */
    @Test
    public void testEqualsTrue() {
        System.out.println("equals true");
        Bainha instance = new Bainha("Bainha Padrão", "Standard Cladding", 1.25);
        Bainha obj = new Bainha(instance);
        
        boolean result = instance.equals(obj);
        assertEquals(true, result);
    }
    
    /**
     * Test of equals method, of class Bainha.
     */
    @Test
    public void testEqualsFalse() {
        System.out.println("equals false");
        Bainha instance = new Bainha("Bainha Padrão", "Standard Cladding", 1.25);
        Bainha obj = new Bainha(instance);
        obj.setIndex(1.32);
        
        boolean result = instance.equals(obj);
        assertEquals(false, result);
    }
    
}
