/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fibraoptica.model;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Sara Freitas
 */
public class FibraOpticaTest {
    
    public FibraOpticaTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of setAngIncidencia method, of class FibraOptica.
     */
    @Test
    public void testGetSetAngIncidencia() {
        System.out.println("get e set AngIncidencia");
        FibraOptica instance = new FibraOptica();
        double angIncidencia = 4.0;
        instance.setAngIncidencia(angIncidencia);
        double result= instance.getAngIncidencia();
        assertEquals(angIncidencia, result);
    }

    /**
     * Test of setNucleo method, of class FibraOptica.
     */
    @Test
    public void testGetSetNucleo() {
        System.out.println("get e set Nucleo");
        Nucleo n = new Nucleo ("pt", "eng", 2.5);
        FibraOptica instance = new FibraOptica();
        instance.setNucleo(n);
        Nucleo result= instance.getNucleo();
        assertEquals(n, result);
    }

    /**
     * Test of setBainha method, of class FibraOptica.
     */
    @Test
    public void testGetSetBainha() {
        System.out.println("get e setBainha");
        Bainha b = new Bainha("pt","eng",2.5);
        FibraOptica instance = new FibraOptica();
        instance.setBainha(b);
        Bainha result= instance.getBainha();
        assertEquals(b, result);
    }

    /**
     * Test of setMeioAmbiente method, of class FibraOptica.
     */
    @Test
    public void testGetSetMeioAmbiente() {
        System.out.println("get e setMeioAmbiente");
        MeioAmbiente ma = new MeioAmbiente("pt","eng",2.5);
        FibraOptica instance = new FibraOptica();
        instance.setMeioAmbiente(ma);
        MeioAmbiente result=instance.getMeioAmbiente();
        assertEquals(ma, result);
    }

    

    /**
     * Test of getAngCritico method, of class FibraOptica.
     */
    @Test
    public void testGetSetAngCritico() {
        System.out.println("get e AngCritico");
        FibraOptica instance = new FibraOptica();
        double expResult = 0.0;
        double result = instance.getAngCritico();
        assertEquals(result,expResult);
        
    }

    /**
     * Test of getAngAceitacao method, of class FibraOptica.
     */
    @Test
    public void testGetAngAceitacao() {
        System.out.println("getAngAceitacao");
        FibraOptica instance = new FibraOptica();
        double expResult = 0.0;
        double result = instance.getAngAceitacao();
        assertEquals(expResult, result);
        
    }

    /**
     * Test of getAberturaNumerica method, of class FibraOptica.
     */
    @Test
    public void testGetAberturaNumerica() {
        System.out.println("getAberturaNumerica");
        FibraOptica instance = new FibraOptica();
        double expResult = 0.0;
        double result = instance.getAberturaNumerica();
        assertEquals(expResult, result);
       
    }

    /**
     * Test of getConeAceitacao method, of class FibraOptica.
     */
    @Test
    public void testGetConeAceitacao() {
        System.out.println("getConeAceitacao");
        FibraOptica instance = new FibraOptica();
        double expResult = 0.0;
        double result = instance.getConeAceitacao();
        assertEquals(expResult, result);
        
    }

      /**
     * Test of equals method, of class Fibras Opticas.
     */
    @Test
    public void testEqualsTrue() {
        System.out.println("equals true");
        Nucleo n= new Nucleo("pt","eng",1.2);
        Bainha b= new Bainha("pt","eng",1.2);
        MeioAmbiente ma= new MeioAmbiente("pt","eng",1.2);
        FibraOptica instance = new FibraOptica(n,b,ma);
        FibraOptica obj = new FibraOptica(instance);
        
        boolean result = instance.equals(obj);
        assertEquals(true, result);
    }
    
    /**
     * Test of equals method, of class Fibras Opticas.
     */
    @Test
    public void testEqualsFalse() {
        System.out.println("equals false");
        Nucleo n= new Nucleo("pt","eng",1.2);
        Bainha b= new Bainha("pt","eng",1.2);
        MeioAmbiente ma= new MeioAmbiente("pt","eng",1.2);
        FibraOptica instance = new FibraOptica(n,b,ma);
        FibraOptica obj = new FibraOptica(instance);
        Bainha bai=new Bainha ("eng","pt",3.5);
        obj.setBainha(bai);
        
        boolean result = instance.equals(obj);
        assertEquals(false, result);
    }  
    
}
