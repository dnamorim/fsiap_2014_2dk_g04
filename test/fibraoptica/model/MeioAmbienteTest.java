/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fibraoptica.model;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author dnamorim
 */
public class MeioAmbienteTest {
    
    public MeioAmbienteTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getNomePT method, of class MeioAmbiente.
     */
    @Test
    public void testGetSetNomePT() {
        System.out.println("get e Set NomePT");
        MeioAmbiente instance = new MeioAmbiente();
        String value = "Vazio";
        instance.setNomePT(value);
        
        String result = instance.getNomePT();
        assertEquals(value, result);
    }

    /**
     * Test of getNomeEng method, of class MeioAmbiente.
     */
    @Test
    public void testGetSetNomeEng() {
        System.out.println("get e Set NomeEng");
        MeioAmbiente instance = new MeioAmbiente();
        String value = "Vacuum";
        instance.setNomeEng(value);
        
        String result = instance.getNomeEng();
        assertEquals(value, result);
    }

    /**
     * Test of getIndex method, of class MeioAmbiente.
     */
    @Test
    public void testGetIndex() {
        System.out.println("get e SetIndex");
        MeioAmbiente instance = new MeioAmbiente();
        double value = 1;
        instance.setIndex(value);
        double result = instance.getIndex();
        assertEquals(value, result, 0.000);
    }

    /**
     * Test of toString method, of class MeioAmbiente.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        MeioAmbiente instance = new MeioAmbiente("Vazio", "Vacuum", 1);
        String expResult = String.format("Meio Ambiente: %s (%s). Índice Refracção: %.4f" , instance.getNomePT(), instance.getNomeEng(), instance.getIndex() );
        
        String result = instance.toString();
        assertEquals(expResult, result);
    }

    /**
     * Test of equals method, of class MeioAmbiente.
     */
    @Test
    public void testEqualsTrue() {
        System.out.println("equals true");
        MeioAmbiente instance = new MeioAmbiente("Vazio", "Vacuum", 1);
        MeioAmbiente obj = new MeioAmbiente(instance);
        
        boolean result = instance.equals(obj);
        assertEquals(true, result);
    }
    
    /**
     * Test of equals method, of class MeioAmbiente.
     */
    @Test
    public void testEqualsFalse() {
        System.out.println("equals false");
        MeioAmbiente instance = new MeioAmbiente("Vazio", "Vacuum", 1);
        MeioAmbiente obj = new MeioAmbiente(instance);
        obj.setIndex(1.2);
        
        boolean result = instance.equals(obj);
        assertEquals(false, result);
    }
    
}
