/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fibraoptica.model;

import java.io.IOException;
import javax.xml.parsers.ParserConfigurationException;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.xml.sax.SAXException;

/**
 *
 * @author dnamorim
 */
public class XMLRefValuesParserTest {
    
    public XMLRefValuesParserTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of importCores method, of class XMLRefValuesParser.
     */
    @Test
    public void testImportCores() throws ParserConfigurationException, SAXException, IOException {
        System.out.println("importCores");
        AppData instanceAD = new AppData();
        AppData appdata = new AppData();
        appdata.addNucleo(new Nucleo("Núcleo", "Core", 1));
        XMLRefValuesParser instance = new XMLRefValuesParser(instanceAD, "xmlParserTestFile.xml");
        instance.importCores();
        assertEquals(appdata.getListNucleos(), instanceAD.getListNucleos());
    }

    /**
     * Test of importCladdings method, of class XMLRefValuesParser.
     */
    @Test
    public void testImportCladdings() throws ParserConfigurationException, SAXException, IOException {
        System.out.println("importCladdings");
        AppData instanceAD = new AppData();
        AppData appdata = new AppData();
        appdata.addBainha(new Bainha("Bainha", "Cladding", 1));
        XMLRefValuesParser instance = new XMLRefValuesParser(instanceAD, "xmlParserTestFile.xml");
        instance.importCladdings();
        assertEquals(appdata.getListBainhas(), instanceAD.getListBainhas());
    }

    /**
     * Test of importEnvironments method, of class XMLRefValuesParser.
     */
    @Test
    public void testImportEnvironments() throws ParserConfigurationException, SAXException, IOException {
        System.out.println("importEnvironments");
        AppData instanceAD = new AppData();
        AppData appdata = new AppData();
        appdata.addMeioAmbiente(new MeioAmbiente("Meio Ambiente", "Environment", 1));
        XMLRefValuesParser instance = new XMLRefValuesParser(instanceAD, "xmlParserTestFile.xml");
        instance.importEnvironments();
        assertEquals(appdata.getListMeiosAmbiente(), instanceAD.getListMeiosAmbiente());
    }
    
}
