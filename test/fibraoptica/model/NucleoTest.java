/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fibraoptica.model;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author dnamorim
 */
public class NucleoTest {
    
    public NucleoTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getNomePT method, of class Nucleo.
     */
    @Test
    public void testGetSetNomePT() {
        System.out.println("get e Set NomePT");
        Nucleo instance = new Nucleo();
        String value = "Standard Core";
        instance.setNomePT(value);
        
        String result = instance.getNomePT();
        assertEquals(value, result);
    }

    /**
     * Test of getNomeEng method, of class Nucleo.
     */
    @Test
    public void testGetSetNomeEng() {
        System.out.println("get e Set NomeEng");
        Nucleo instance = new Nucleo();
        String value = "Standard Core";
        instance.setNomeEng(value);
        
        String result = instance.getNomeEng();
        assertEquals(value, result);
    }

    /**
     * Test of getIndex method, of class Nucleo.
     */
    @Test
    public void testGetIndex() {
        System.out.println("get e SetIndex");
        Nucleo instance = new Nucleo();
        double value = 1.423;
        instance.setIndex(value);
        double result = instance.getIndex();
        assertEquals(value, result, 0.000);
    }

    /**
     * Test of toString method, of class Nucleo.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        Nucleo instance = new Nucleo("Núcleo Padrão", "Standard Core", 1.425);
        String expResult = String.format("Núcleo: %s (%s). Índice Refracção: %.4f" , instance.getNomePT(), instance.getNomeEng(), instance.getIndex() );
        
        String result = instance.toString();
        assertEquals(expResult, result);
    }

    /**
     * Test of equals method, of class Nucleo.
     */
    @Test
    public void testEqualsTrue() {
        System.out.println("equals true");
        Nucleo instance = new Nucleo("Núcleo Padrão", "Standard Core", 1.425);
        Nucleo obj = new Nucleo(instance);
        
        boolean result = instance.equals(obj);
        assertEquals(true, result);
    }
    
    /**
     * Test of equals method, of class Nucleo.
     */
    @Test
    public void testEqualsFalse() {
        System.out.println("equals false");
        Nucleo instance = new Nucleo("Núcleo Padrão", "Standard Core", 1.425);
        Nucleo obj = new Nucleo(instance);
        obj.setIndex(1.432);
        
        boolean result = instance.equals(obj);
        assertEquals(false, result);
    }
    
}
