/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fibraoptica.model;

import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author dnamorim
 */
public class AppDataTest {
    
    public AppDataTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of addNucleo method, of class AppData.
     */
    @Test
    public void testAddNucleoNotExists() {
        System.out.println("addNucleo not exists");
        Nucleo n = new Nucleo();
        AppData instance = new AppData();
        boolean result = instance.addNucleo(n);
        
        assertEquals(true, result);
    }
    
    /**
     * Test of addNucleo method, of class AppData.
     */
    @Test
    public void testAddNucleoExists() {
        System.out.println("addNucleo exists");
        Nucleo n1 = new Nucleo("A", "A", 1.23);
        AppData instance = new AppData();
        instance.addNucleo(n1);
        boolean result = instance.addNucleo(new Nucleo(n1));
        
        assertEquals(false, result);
    }

     /**
     * Test of addNucleo method, of class AppData.
     */
    @Test
    public void testAddNucleoNull() {
        System.out.println("addNucleo null");
        Nucleo n = null;
        AppData instance = new AppData();
        boolean result = instance.addNucleo(n);
        
        assertEquals(false, result);
    }
        
    /**
     * Test of addBainha method, of class AppData.
     */
    @Test
    public void testAddBainhaNotExists() {
        System.out.println("addBainha not exists");
        Bainha b = new Bainha("B", "b", 1.23);
        AppData instance = new AppData();
        
        boolean result = instance.addBainha(b);
        assertEquals(true, result);
    }

    /**
     * Test of addBainha method, of class AppData.
     */
    @Test
    public void testAddBainhaExists() {
        System.out.println("addBainha exists");
        Bainha b = new Bainha("B", "b", 1.23);
        AppData instance = new AppData();
        
        instance.addBainha(b);
        boolean result = instance.addBainha(new Bainha(b));
        assertEquals(false, result);
    }
    
    /**
     * Test of addBainha method, of class AppData.
     */
    @Test
    public void testAddBainhaNull() {
        System.out.println("addBainha null");
        Bainha b = null;
        AppData instance = new AppData();
        
        instance.addBainha(b);
        boolean result = instance.addBainha(b);
        assertEquals(false, result);
    }
    
    /**
     * Test of addMeioAmbiente method, of class AppData.
     */
    @Test
    public void testAddMeioAmbienteNotExists() {
        System.out.println("addMeioAmbiente not exists");
        MeioAmbiente ma = new MeioAmbiente("MA", "ma", 1);
        AppData instance = new AppData();
        
        boolean result = instance.addMeioAmbiente(ma);
        assertEquals(true, result);
    }
    
    /**
     * Test of addMeioAmbiente method, of class AppData.
     */
    @Test
    public void testAddMeioAmbienteExists() {
        System.out.println("addMeioAmbiente exists");
        MeioAmbiente ma = new MeioAmbiente("MA", "ma", 1);
        AppData instance = new AppData();
        
        instance.addMeioAmbiente(ma);
        boolean result = instance.addMeioAmbiente(new MeioAmbiente(ma));
        assertEquals(false, result);
    }
    
    /**
     * Test of addMeioAmbiente method, of class AppData.
     */
    @Test
    public void testAddMeioAmbienteNull() {
        System.out.println("addMeioAmbiente null");
        MeioAmbiente ma = null;
        AppData instance = new AppData();
        
        boolean result = instance.addMeioAmbiente(ma);
        assertEquals(false, result);
    }

    /**
     * Test of getListNucleos method, of class AppData.
     */
    @Test
    public void testGetListNucleos() {
        System.out.println("getListNucleos");
        AppData instance = new AppData();
        instance.addNucleo(new Nucleo("A1", "a1", 1));
        instance.addNucleo(new Nucleo("A2", "a2", 2));
        
        List<Nucleo> expResult = new ArrayList<>();
        expResult.add(new Nucleo("A1", "a1", 1));
        expResult.add(new Nucleo("A2", "a2", 2));
        
        assertEquals(expResult, instance.getListNucleos());
    }

    /**
     * Test of getListNucleos method, of class AppData.
     */
    @Test
    public void testGetListBainhas() {
        System.out.println("getListBainhas");
        AppData instance = new AppData();
        instance.addBainha(new Bainha("A1", "a1", 1));
        instance.addBainha(new Bainha("A2", "a2", 2));
        
        List<Bainha> expResult = new ArrayList<>();
        expResult.add(new Bainha("A1", "a1", 1));
        expResult.add(new Bainha("A2", "a2", 2));
        
        assertEquals(expResult, instance.getListBainhas());
    }

    /**
     * Test of getListMeiosAmbiente method, of class AppData.
     */
    @Test
    public void testGetListMeiosAmbiente() {
        System.out.println("getListMeiosAmbiente");
        AppData instance = new AppData();
        instance.addMeioAmbiente(new MeioAmbiente("A1", "a1", 1));
        instance.addMeioAmbiente(new MeioAmbiente("A2", "a2", 2));
        
        List<MeioAmbiente> expResult = new ArrayList<>();
        expResult.add(new MeioAmbiente("A1", "a1", 1));
        expResult.add(new MeioAmbiente("A2", "a2", 2));
        
        assertEquals(expResult, instance.getListMeiosAmbiente());
    } 

    /**
     * Test of isEmpty method, of class AppData.
     */
    @Test
    public void testIsEmptyTrue() {
        System.out.println("isEmpty true");
        AppData instance = new AppData();
        
        assertEquals(true, instance.isEmpty());
    }
    
        /**
     * Test of isEmpty method, of class AppData.
     */
    @Test
    public void testIsEmptyFalse() {
        System.out.println("isEmpty false");
        AppData instance = new AppData();
        instance.addBainha(new Bainha());
        
        assertEquals(false, instance.isEmpty());
    }

}
